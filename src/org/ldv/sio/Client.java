package org.ldv.sio;


public class Client {
  private String nom;
  private String prenom;
  private String rue;
  private String codePostal;
  private String ville;

  public Client(String nom, String prenom, String rue, String codePostal, String ville) {
    this.nom = nom;
    this.prenom = prenom;
    this.rue = rue;
    this.codePostal = codePostal;
    this.ville = ville;
  }



  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getRue() { return rue; }

  public void setRue(String rue) { this.rue = rue; }

  public String getCodePostal() { return codePostal; }

  public void setCodePostal(String codePostal) { this.codePostal = codePostal; }

  public String getVille() { return ville; }

  public void setVille(String ville) { this.ville = ville; }


  @Override
  public String toString() {
    return "Client {" +
            "nom='" + nom + '\'' +
            ", rue='" + rue + '\'' +
            ", code Postal='" + codePostal + '\'' +
            ", ville='" + ville + '\'' +
            '}';
  }
}
